package renatsayf.example_result_asynctask;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements MyAsyncTask.AsyncTask1.IAsyncResult1, MyAsyncTask.AsyncTask2.IAsyncResult2
{
    private TextView textView;
    private ProgressBar progressBar;
    private Button btnStart;
    private MyAsyncTask.AsyncTask1 asyncTask1;
    private MyAsyncTask.AsyncTask2 asyncTask2;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews()
    {
        textView = (TextView) findViewById(R.id.textView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnStart = (Button) findViewById(R.id.btn_Start);
        btnStart_OnClick();
    }

    private void btnStart_OnClick()
    {
        btnStart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                textView.setText("Выполняем...");
                progressBar.setVisibility(View.VISIBLE);
                new LockOrientation(MainActivity.this).lock();
                asyncTask1 = new MyAsyncTask.AsyncTask1(MainActivity.this);
                asyncTask1.execute();
            }
        });
    }


    @Override
    public void asyncTask1Result(String param)
    {
        textView.setText(param);
        progressBar.setVisibility(View.GONE);
        asyncTask2 = new MyAsyncTask.AsyncTask2(MainActivity.this)
        {
            @Override
            public String abstrMethod(String param)
            {
                Toast.makeText(MainActivity.this,"Сработал абстрактный метод, param = "+param,Toast.LENGTH_SHORT).show();
                return null;
            }
        };
        asyncTask2.execute();
        progressBar.setVisibility(View.VISIBLE);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    private String Key_Text;
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(Key_Text, textView.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        textView.setText(savedInstanceState.getString(Key_Text));
    }

    @Override
    public void asyncTask2Result(String param)
    {
        textView.setText(param);
        progressBar.setVisibility(View.GONE);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }
}
