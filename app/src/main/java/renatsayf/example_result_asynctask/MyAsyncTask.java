package renatsayf.example_result_asynctask;

import android.os.AsyncTask;

/**
 * Created by Ренат on 04.08.2016.
 */
public class MyAsyncTask
{
    public static class AsyncTask1 extends AsyncTask<String, String, String>
    {
        private IAsyncResult1 callback;

        public AsyncTask1(final IAsyncResult1 callback)
        {
            this.callback = callback;
        }

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                Thread.sleep(5000);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return "Первая задача выполнена";
        }

        @Override
        protected void onPostExecute(String o)
        {
            if(callback!=null)
                callback.asyncTask1Result(o);
        }



        public interface IAsyncResult1
        {
            void asyncTask1Result(String param);
        }
    }




    public abstract static class AsyncTask2 extends AsyncTask<String, String, String>
    {
        private IAsyncResult2 callback;
        public interface IAsyncResult2
        {
            public void asyncTask2Result(String param);
        }
        public AsyncTask2(final IAsyncResult2 callback)
        {
            this.callback = callback;
        }

        public abstract String abstrMethod(String param);

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                Thread.sleep(5000);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return "Вторая задача выполнена";
        }

        @Override
        protected void onPostExecute(String s)
        {
            if(callback!=null) callback.asyncTask2Result(s);
            abstrMethod(s);
        }
    }

}
